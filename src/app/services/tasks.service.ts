import { Injectable } from '@angular/core';

import { Task } from '../models/task';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  tasks = [
    {'id': 1, 'name': 'Learn Angular', 'dueDate': new Date('2022-04-21'), 'completed': false},
    {'id': 2, 'name': 'Learn Symfony', 'dueDate': new Date('2022-05-21'), 'completed': false},
    {'id': 3, 'name': 'Document the project', 'dueDate': new Date('2022-04-30'), 'completed': false},
    {'id': 4, 'name': 'Think of a project', 'dueDate': new Date('2022-04-15'), 'completed': true},
    {'id': 5, 'name': 'Drink water', 'dueDate': new Date('2022-04-21'), 'completed': true},
  ];

  getTasks(): Task[] {
    return this.tasks;
  }

  deleteTask(id: Number): void {
    this.tasks = this.tasks.filter(e => e.id !== id);

    /* delete from the server */
    console.log('deleting...');
  }

  updateTask(task: Task): void {
    /* update on the server */
    console.log('updating...');
  }
}
