import { Component, OnInit, Input } from '@angular/core';

import { TasksService} from '../../services/tasks.service';
import { Task } from '../../models/task';

@Component({
  selector: 'app-task-counter',
  templateUrl: './task-counter.component.html',
  styleUrls: ['./task-counter.component.scss']
})
export class TaskCounterComponent implements OnInit {

  tasks: Task[] = [];

  remainingTasks: Number = 0;

  constructor(private tasksService: TasksService) { }

  ngOnInit(): void {
    this.getTasks();
    this.getRemainingTasks();
  }

  getTasks(): void {
    this.tasks = this.tasksService.getTasks();
  }

  getRemainingTasks(): void {
    this.remainingTasks = this.tasks.filter(e => e.completed === true).length;
  }

  refresh(): void {
    this.getTasks();
    this.getRemainingTasks();
  }
}
