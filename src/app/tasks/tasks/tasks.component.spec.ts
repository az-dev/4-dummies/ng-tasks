import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TasksComponent } from './tasks.component';

describe('TasksComponent', () => {
  let component: TasksComponent;
  let fixture: ComponentFixture<TasksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TasksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should be five tasks', () => {
    expect(component.tasks.length).toBe(5);
  });

  it('should be able to delete the third task (id=3)', () => {
    const button_delete: HTMLElement = fixture.debugElement.nativeElement.querySelectorAll("button[name='delete-task']")[2];

    button_delete.click();

    fixture.detectChanges();

    let task_deleted = component.tasks.find(e => e.id === 3);

    expect(task_deleted).toBeUndefined();
  });

  it('should be able to delete the fifth task (id=5)', () => {
    const button_delete: HTMLElement = fixture.debugElement.nativeElement.querySelectorAll("button[name='delete-task']")[4];

    button_delete.click();

    fixture.detectChanges();

    let task_deleted = component.tasks.find(e => e.id === 5);

    expect(task_deleted).toBeUndefined();
  });

  it('should be able to toggle the fourth task (id=4) true => false', () => {
    const button_toggle: HTMLElement = fixture.debugElement.nativeElement.querySelectorAll("button[name='toogle-completion']")[3];

    button_toggle.click();

    fixture.detectChanges();

    let fourth_task = component.tasks.find(e => e.id === 4)!;

    expect(fourth_task.completed).toBeFalse();
  });

  it('should be able to toggle the fourth task (id=4) true => false => true', () => {
    const button_toggle: HTMLElement = fixture.debugElement.nativeElement.querySelectorAll("button[name='toogle-completion']")[3];

    button_toggle.click();

    fixture.detectChanges();

    button_toggle.click();

    fixture.detectChanges();

    let fourth_task = component.tasks.find(e => e.id === 4)!;

    expect(fourth_task.completed).toBeTrue();
  });
});
