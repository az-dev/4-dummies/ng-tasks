import { Component, OnInit } from '@angular/core';

import { TasksService } from '../../services/tasks.service';
import { Task } from '../../models/task';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  tasks: Task[] = [];

  constructor(private tasksService: TasksService) { }

  ngOnInit(): void {
    this.getTasks();
  }

  getTasks(): void {
    this.tasks = this.tasksService.getTasks();
  }

  deleteTask(task: Task): void {
    this.tasksService.deleteTask(task.id);

    this.getTasks();
  }

  toggleCompletionTask(task: Task): void {
    task.completed = !task.completed,
    this.tasksService.updateTask(task);
  }

}
