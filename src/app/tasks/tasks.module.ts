import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TasksComponent } from './tasks/tasks.component';
import { TaskCounterComponent } from './task-counter/task-counter.component';

@NgModule({
  declarations: [
    TasksComponent,
    TaskCounterComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [TasksComponent]
})
export class TasksModule { }
