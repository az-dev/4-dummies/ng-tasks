import { Task } from './task';

describe('TaskModel', () => {
  let task: Task;

  beforeEach(() => {
    task = new Task(42, 'Sample Task', new Date('2022-05-01'), true);
  });

  it('should create an instance', () => {
    expect(task).toBeTruthy();
  });

  it('should be the task 42', () => {
    expect(task.id).toBe(42);
  });
});
